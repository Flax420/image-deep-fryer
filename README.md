# Image Deep Fryer
A program made in Rust that ruins images. It uses GTK for GUI and Raster to achieve this.  
# Example output
![Example](/images/example_fried.jpeg)