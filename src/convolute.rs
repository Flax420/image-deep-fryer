extern crate gtk;
extern crate raster;
use raster::*;
pub fn deepfry(infile: &String, outfile: &String) {
    let image = raster::open(&infile).unwrap();
    let image2 = raster::open(&"images/leemao.png".to_string()).unwrap();
    let image3 = raster::open(&"images/te4.jpg".to_string()).unwrap();
    let mut image_with_emoji = editor::blend(
        &image,
        &image2,
        BlendMode::Normal,
        1.0,
        PositionMode::Center,
        0,
        0,
    )
    .unwrap();
    let matrix: [[i32; 3]; 3] = [[1, 0, 0], [0, 0, 0], [0, 0, 1]];
    filter::sharpen(&mut image_with_emoji).unwrap();
    filter::emboss(&mut image_with_emoji).unwrap();
    raster::filter::convolve(&mut image_with_emoji, matrix, 1).unwrap();
    let gang_sign = editor::blend(
        &image_with_emoji,
        &image3,
        BlendMode::Normal,
        1.0,
        PositionMode::BottomRight,
        0,
        0,
    )
    .unwrap();
    raster::save(&gang_sign, &outfile).unwrap();
}
