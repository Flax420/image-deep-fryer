#![windows_subsystem = "windows"]
extern crate gtk;
extern crate raster;
mod convolute;
use convolute::deepfry;
use gtk::prelude::*;
use std::sync::Arc;
use std::thread;

fn main() {
    if gtk::init().is_err() {
        println!("Failed to initialize GTK.");
        return;
    }
    // Sets up GUI
    // Loads glade file, sets up builder, gets elements
    let glade_src = include_str!("../gui/main.glade");
    let builder = gtk::Builder::new_from_string(glade_src);
    let window: gtk::Window = builder.get_object("window1").unwrap();
    let button: gtk::Button = builder.get_object("button1").unwrap();
    let input1: gtk::Entry = builder.get_object("input1").unwrap();
    let input2: gtk::Entry = builder.get_object("input2").unwrap();

    // Button click event
    button.connect_clicked(move |_| {
        let label: gtk::Label = builder.get_object("label4").unwrap();
        // Setting label
        label.set_markup("Deepfrying...");
        // Setting up data to send to secondary thread
        let to_send: Vec<String> = vec![input1.get_text().unwrap(), input2.get_text().unwrap()];
        // Thread safe pointers
        let send_ptr = Arc::new(to_send);
        let done_ptr = Arc::new(true);
        let done_ptr_weak = Arc::downgrade(&done_ptr);
        thread::spawn(move || {
            let rec_ptr = Arc::clone(&send_ptr);
            deepfry(&rec_ptr[0], &rec_ptr[1]);
            // Drops done_ptr making done_ptr_weak null since it's a weak pointer
            drop(done_ptr);
        });
        // this runs in the gtk main loop every 100 miliseconds
        // this could probably be substituted for a coroutine through coio
        let _test_if_done = gtk::timeout_add(100, move || {
            // If done_ptr has not been dropped
            if done_ptr_weak.upgrade().is_some() {
                gtk::Continue(true)
            } else {
                label.set_markup("Ready!");
                gtk::Continue(false)
            }
        });
    });

    window.show_all();

    // Handle closing of the window.
    window.connect_delete_event(|_, _| {
        // Stop the main loop.
        gtk::main_quit();
        // Let the default handler destroy the window.
        Inhibit(false)
    });
    // GTK main loop
    gtk::main();
}
